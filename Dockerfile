FROM centos:7
RUN yum -y update

# Copy the bridge RPM package, install it, then remove it from the image
#COPY tableau/TableauBridge-20233.23.1017.0948.x86_64.rpm ./
#Install Tableau Bridge
#RUN ACCEPT_EULA=y yum install -y ./TableauBridge-20233.23.1017.0948.x86_64.rpm


#  MySQL driver RPM copied into the directory amd install it
#COPY drivers/mysql-connector-odbc-8.2.0-1.el7.x86_64.rpm ./
#RUN yum install -y ./mysql-connector-odbc-8.2.0-1.el7.x86_64.rpm



# copy  postgres JDBC driver
#COPY drivers/postgresql-42.3.7.jar /opt/tableau/tableau_driver/jdbc/

# COPY and install
#COPY drivers/AmazonRedshiftODBC-64-bit-1.5.7.1007-1.x86_64.rpm ./
#RUN yum install -y unixODBC
#RUN yum --nogpgcheck localinstall -y AmazonRedshiftODBC-64-bit-1.5.7.1007-1.x86_64.rpm
#RUN odbcinst -i -d -f /opt/amazon/redshiftodbc/Setup/odbcinst.ini

#oracle odbc
#COPY drivers/ojdbc11.jar /opt/tableau/tableau_driver/jdbc/

# tocken file
#COPY token.txt /home/tableau/token.txt
#COPY <tokenpath> /home/tableau/token. # token from CI/CD environment variable 

# Certificates
#RUN yum install -y ca-certificates
#COPY certificates/eu-west-1a.online.tableau.com.crt /etc/pki/ca-trust/source/anchors
#COPY certificates/Roche_ca.crt /etc/pki/ca-trust/source/anchors
#RUN cp /etc/pki/tls/certs/ca-bundle.crt /etc/pki/tls/certs/ca-bundle.crt.bkp
#RUN update-ca-trust
#RUN update-ca-trust force-enable
# add user and permision to run tableauB
RUN useradd -m tableaub
#RUN chmod -R 777 /opt/tableau/tableau_bridge
# add user to sudores 
#RUN yum install -y sudo
#RUN echo "tableaub ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

